const divCurrentDate = document.getElementById("current_date");

const timeChecking = 60 * 1000;
let previuosHours = 0;

let date = new Date();
let hours = date.getHours();

pastCurrentDateToDOM(date, hours);

setTimeout(function pastCurrentDateToDOMInterval() {
  date = new Date();
  hours = date.getHours();

  if (hours !== previuosHours) {
    previuosHours = hours;
    pastCurrentDateToDOM(date, hours);
  }
  setTimeout(pastCurrentDateToDOMInterval, timeChecking);
}, timeChecking);

function pastCurrentDateToDOM(date, hours) {
  let currentHours = `${hours < 10 ? "0" + hours : hours} ${
    hours === 1 ? "hour" : "hours"
  }`;

  let currentDay = `${date.getDate()} (${date.toLocaleString("en-GB", {
    weekday: "long",
  })})`;

  let currentMounth = `${date.toLocaleString("en-GB", {
    month: "long",
  })}`;

  let currentYear = `${date.getFullYear()} year`;

  createDOMelements(currentHours, currentDay, currentMounth, currentYear);
}

function createDOMelements(
  currentHours,
  currentDay,
  currentMounth,
  currentYear
) {
  let divHours = document.createElement("div");
  let divDay = document.createElement("div");
  let divMounth = document.createElement("div");
  let divYear = document.createElement("div");

  divHours.classList.add("current", "current_hour");
  divDay.classList.add("current", "current_day");
  divMounth.classList.add("current", "current_mounth");
  divYear.classList.add("current", "current_year");

  divHours.append(currentHours);
  divDay.append(currentDay);
  divMounth.append(currentMounth);
  divYear.append(currentYear);

  if (divCurrentDate) {
    [...divCurrentDate.childNodes].map((child) => child.remove());

    divCurrentDate.append(divHours);
    divCurrentDate.append(divDay);
    divCurrentDate.append(divMounth);
    divCurrentDate.append(divYear);
  }
}
